#!/bin/bash

# FAIL in case of any error
set -e
function exittrap {
    echo "$0 failed"
    exit 1
}
trap exittrap ERR INT TERM
set -x

# Environment
ARCH=$1
NUMPROCS=${2:-4}
NUMTHRDS=${3:-2}
BIOEM=../build/bioEM

module load gcc/$GCC_VER impi fftw-mpi

export OMP_NUM_THREADS=$NUMTHRDS
export OMP_PLACES=cores
export OMP_STACKSIZE=128M

cat /proc/cpuinfo > environment
env >> environment
> ang_prob

cp -r BioEM-tutorials/Tutorial_BioEM test2
cd test2

# Compare results with a certain numerical precision
module load numdiff
NUMDIFF=numdiff

# Running tests
echo "@testing help output (error allowed)"
$BIOEM --help || true

echo "@testing debugging options"
BIOEM_DEBUG_OUTPUT=2 BIOEM_DEBUG_BREAK=2 BIOEM_DEBUG_NMAPS=1 $BIOEM --Inputfile Param_Input --Modelfile Model_Text --Particlesfile Text_Image_Form

echo "@testing prior probabilities"
$BIOEM --Modelfile Model_Text --Particlesfile Text_Image_Form --Inputfile Param_Input_Priors --ReadOrientation Euler_Angle_List_Prior
$NUMDIFF --relative=1e-2 Output_Probabilities test_outputs/Output_Probabilities_prior

echo "@testing print map model"
$BIOEM --Modelfile Model.pdb --ReadPDB --PrintBestCalMap Param_Print_MAP
# Random generator is involved, so there is no sence to compare the error
#$NUMDIFF --relative=1e-4 BESTMAP test_outputs/BESTMAP

echo "@testing print posterior as a function of orientations"
mpiexec -n $NUMPROCS $BIOEM --Inputfile Param_Input_WritePAng --Modelfile Model_Text --Particlesfile Text_Image_Form
cat ANG_PROB > ang_prob
$NUMDIFF --overview ANG_PROB test_outputs/ANG_PROB || true
$NUMDIFF --relative=1e-2 ANG_PROB test_outputs/ANG_PROB

echo "@successfully finished testing several specific options"
