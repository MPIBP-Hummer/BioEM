#!/bin/bash

INPUT_DIR=$1
OUTPUT_DIR=$2

module purge
module load gcc/$GCC_VER
module load lcov

mkdir $OUTPUT_DIR
lcov --no-checksum --capture --directory $INPUT_DIR --output-file coverage.info
lcov -r coverage.info /usr/*    --output-file coverage.info
lcov -r coverage.info /usr/include/*    --output-file coverage.info
lcov -r coverage.info *$GCC_VER*    --output-file coverage.info
genhtml coverage.info --output-directory $OUTPUT_DIR
