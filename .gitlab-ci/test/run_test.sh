#!/bin/bash

env

ARCH=$1
NUMPROCS=${2:-4}
NUMTHRDS=${3:-2}

module purge
module load gcc/$GCC_VER impi fftw-mpi

export OMP_NUM_THREADS=$NUMTHRDS
export OMP_PLACES=cores
export OMP_STACKSIZE=128M

cp -r BioEM-tutorials/Tutorial_BioEM test1
cd test1
mpiexec -n $NUMPROCS ../build/bioEM --Inputfile Param_Input --Modelfile Model_Text --Particlesfile Text_Image_Form
