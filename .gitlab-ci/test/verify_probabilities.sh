#!/bin/bash

# FAIL in case of any error
set -e
function exittrap {
    echo "$0 failed"
    exit 1
}
trap exittrap ERR INT TERM
set -x

# Environment
ARCH=$1
NUMPROCS=${2:-4}
NUMTHRDS=${3:-2}
BIOEM=../build/bioEM

module load gcc/$GCC_VER impi fftw-mpi

export OMP_NUM_THREADS=$NUMTHRDS
export OMP_PLACES=cores
export OMP_STACKSIZE=128M

cat /proc/cpuinfo > environment
env >> environment
> all_probabilities

cp -r BioEM-tutorials/Tutorial_BioEM test3
cd test3

# Compare results with a certain numerical precision
module load numdiff
NUMDIFF=numdiff

# Running experiments
for algo in 1 2
do
    for gpu in 0
    do
	for proj in 1 $OMP_NUM_THREADS
	do
	    echo "@running probabilities for BIOEM_ALGO=$algo GPU=$gpu BIOEM_PROJ_CONV_AT_ONCE=$proj"
	    BIOEM_ALGO=$algo GPU=$gpu BIOEM_PROJ_CONV_AT_ONCE=$proj BIOEM_DEBUG_OUTPUT=2 mpiexec -n $NUMPROCS $BIOEM --Inputfile Param_Input --Modelfile Model_Text --Particlesfile Text_Image_Form

	    echo "@probabilities for BIOEM_ALGO=$algo GPU=$gpu BIOEM_PROJ_CONV_AT_ONCE=$proj"
	    cat Output_Probabilities
	    cat Output_Probabilities >> all_probabilities

	    echo "@comparing Output_Probabilities test_outputs/Output_Probabilities_ref"
	    $NUMDIFF --overview Output_Probabilities test_outputs/Output_Probabilities_ref || true

	    echo "@verifying Output_Probabilities test_outputs/Output_Probabilities_ref"
	    $NUMDIFF --relative=1e-1 Output_Probabilities test_outputs/Output_Probabilities_ref
	done
    done
done

echo "@successfully finished testing probabilities for both algorithms"
