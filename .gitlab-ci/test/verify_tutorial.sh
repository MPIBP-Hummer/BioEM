#!/bin/bash

# FAIL in case of any error
set -e
function exittrap {
    echo "$0 failed"
    exit 1
}
trap exittrap ERR INT TERM
set -x

module load gcc/$GCC_VER impi fftw-mpi

# Environment
BIOEM=../${1}
TUTORIAL_DIR=$2
echo "BioEM path: ${BIOEM}"
echo "Tutorial directory path: ${TUTORIAL_DIR}"

rm -rf testTutorial
cp -r $TUTORIAL_DIR testTutorial
cd testTutorial

# Running tests
echo "@testing all tutorial options"

echo "@@ Text model - Text Image"
$BIOEM --Inputfile Param_Input --Modelfile Model_Text --Particlesfile Text_Image_Form

echo "@@ PDB Model - Text Image "
$BIOEM --Inputfile Param_Input --Modelfile Model.pdb --ReadPDB --Particlesfile Text_Image_Form

echo "@@ PDB Model - One MRC Image"
$BIOEM --Inputfile Param_Input --Modelfile Model.pdb --ReadPDB --Particlesfile OneImage.mrc --ReadMRC

echo "@@ PDB Model - Multiple MRCs"
$BIOEM --Inputfile Param_Input --Modelfile Model.pdb --ReadPDB --Particlesfile ListMRC --ReadMRC --ReadMultipleMRC

echo "@@ Read Euler angles from file"
$BIOEM --Inputfile Param_Input --Modelfile Model.pdb --ReadPDB --Particlesfile Text_Image_Form --ReadOrientation Euler_Angle_List

echo "@@ Read quaternions from file"
$BIOEM --Inputfile Param_Input_Quat --Modelfile Model.pdb --ReadPDB --Particlesfile Text_Image_Form --ReadOrientation Quat_list_Small

echo "@@ MRC Model - One MRC Image"
echo "Disabled as it is returning many warnings"
#$BIOEM --Inputfile Param_ModelMRC --Modelfile Model_MRC.mrc --ReadModelMRC --Particlesfile particle3.mrcs --ReadMRC --ReadOrientation Quat_list_Small

echo "@@ Input-parameter suggestions"
echo "Disabled as it is too long for the current CI"
#$BIOEM --Inputfile Param_ProRun --Modelfile Model.pdb --ReadPDB --Particlesfile Text_Image_Form --ReadOrientation List_Quat_ProRun

echo "@@ Dump particle-images"
$BIOEM --Inputfile Param_Input --Modelfile Model.pdb --ReadPDB --Particlesfile ListMRC --ReadMRC --ReadMultipleMRC --DumpMaps

echo "@@ Load particle-images"
$BIOEM --Inputfile Param_Input --Modelfile Model.pdb --ReadPDB --LoadMapDump

echo "@@ Dump model"
$BIOEM --Inputfile Param_Input --Modelfile Model.pdb --ReadPDB --Particlesfile ListMRC --ReadMRC --ReadMultipleMRC --DumpModel

echo "@@ Load model"
$BIOEM --Inputfile Param_Input --Particlesfile ListMRC --ReadMRC --ReadMultipleMRC --LoadModelDump

echo "@@ Check model"
$BIOEM --Inputfile Param_Input --Modelfile Model_Text --Particlesfile Text_Image_Form --PrintCOORDREAD

echo "@@ Including prior probabilities"
$BIOEM  --Modelfile Model_Text --Particlesfile Text_Image_Form --Inputfile Param_Input_Priors  --ReadOrientation Euler_Angle_List_Prior

## Dont test for the PrintBestCalMap as it is in the other test (and not in Tutorial anymore)

echo "@successfully finished testing all tutorial options"
