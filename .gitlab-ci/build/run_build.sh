#!/bin/bash

ARCHITECTURE=$1
DEBUG=${2:-no}

module purge
module load cmake

echo "building BioEM with ARCH=$ARCHITECTURE"

case "$ARCHITECTURE" in
intel-mpi)
  module load intel impi fftw-mpi
  rm -rf build/; mkdir build
  cd build
  cmake .. -DFFTWF_LIBRARIES=$FFTW_HOME/lib/libfftw3f.a -DFFTWF_INCLUDE_DIR=$FFTW_HOME/include -DCMAKE_CXX_COMPILER=icpc -DCMAKE_C_COMPILER=icc -DUSE_MPI=on -DUSE_OPENMP=on -DUSE_CUDA=off
  make VERBOSE=1 -j 2
  ;;
gcc-mpi)
  module load gcc/$GCC_VER impi fftw-mpi
  rm -rf build/; mkdir build
  cd build
  cmake .. -DFFTWF_LIBRARIES=$FFTW_HOME/lib/libfftw3f.a -DFFTWF_INCLUDE_DIR=$FFTW_HOME/include -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS_DEBUG='-g -Wall --coverage' -DCMAKE_C_FLAGS_DEBUG='-g -Wall --coverage'
  make VERBOSE=1 -j 2
  ;;
gcc-mpi-cuda)
  module load gcc/$GCC_VER cuda impi fftw-mpi
  rm -rf build/; mkdir build
  cd build
  cmake .. -DFFTWF_LIBRARIES=$FFTW_HOME/lib/libfftw3f.a -DFFTWF_INCLUDE_DIR=$FFTW_HOME/include -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DUSE_CUDA=on -DCUDA_CUDA_LIBRARY=$CUDA_HOME/lib64/stubs/libcuda.so
  make VERBOSE=1 -j 2
  ;;
*)
  echo "ARCHITECTURE $ARCHITECTURE not defined for build"
  exit 1
  ;;
esac

module list
