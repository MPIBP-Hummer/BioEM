#!/bin/bash

# Tests assume 4 nodes, with two GPUs per node.
# If this is not the case, adapt the configuration!

# Input paths
BUILD_DIR=${1}
INPUT_DIR=${2}
datafile=${3}

# Environment variables
export BIOEM="$BUILD_DIR/bioEM"
export FFTALGO=1 # Although this is needed only for BioEM<2.0
export GPUDEVICE=-1
export KMP_AFFINITY=compact,granularity=core,1
export BIOEM_DEBUG_OUTPUT=1

# Environment capture
> $datafile
$HOME/BioEM_project/get_info.sh $datafile

rm -rf Output_Probabilities

###################################
# Exp 1: Large benchmark, with best config
echo "* EXP 1: LARGE BENCH OUTPUT (FACTOR 10)" >> $datafile
#export BIOEM_ALGO=1
export GPU=1
#export GPUWORKLOAD=-1
#export BIOEM_CUDA_THREAD_COUNT=256
export BIOEM_DEBUG_BREAK=1350
#export BIOEM_DEBUG_NMAPS=2000
export OMP_NUM_THREADS=12
#export BIOEM_PROJECTIONS_AT_ONCE=1
export NP=2

# Running
mpiexec -perhost $NP $BIOEM --Inputfile $INPUT_DIR/INPUT_FRH_Sep2016 --Modelfile $INPUT_DIR/Mod_X-ray_PDB --Particlesfile $INPUT_DIR/2000FRH_Part >> $datafile

# Saving probabilities
echo "* EXP 1: PROBABILITIES" >> $datafile
cat Output_Probabilities >> $datafile

# Cleanup
rm -rf Output_Probabilities
unset BIOEM_ALGO GPU GPUWORKLOAD BIOEM_CUDA_THREAD_COUNT BIOEM_DEBUG_BREAK BIOEM_DEBUG_NMAPS OMP_NUM_THREADS BIOEM_PROJECTIONS_AT_ONCE NP

###################################
# Exp 2: Middle benchmark, with best config
echo "* EXP 2: MIDDLE BENCH OUTPUT (FACTOR 10)" >> $datafile
export BIOEM_ALGO=1
export GPU=1
export GPUWORKLOAD=-1
#export BIOEM_CUDA_THREAD_COUNT=256
export BIOEM_DEBUG_BREAK=600
export BIOEM_DEBUG_NMAPS=100
export OMP_NUM_THREADS=12
export BIOEM_PROJECTIONS_AT_ONCE=2
export NP=2

# Running
mpiexec -perhost $NP $BIOEM --Inputfile $INPUT_DIR/INPUT_FRH_Sep2016 --Modelfile $INPUT_DIR/Mod_X-ray_PDB --Particlesfile $INPUT_DIR/2000FRH_Part >> $datafile

# Saving probabilities
echo "* EXP 2: MIDDLE BENCH PROBABILITIES" >> $datafile
cat Output_Probabilities >> $datafile

# Cleanup
rm -rf Output_Probabilities
unset BIOEM_ALGO GPU GPUWORKLOAD BIOEM_CUDA_THREAD_COUNT BIOEM_DEBUG_BREAK BIOEM_DEBUG_NMAPS OMP_NUM_THREADS BIOEM_PROJECTIONS_AT_ONCE NP

###################################
# Exp 3: Small benchmark, with best ALGO=1 config
echo "* EXP 3: SMALL BENCH OUTPUT (FACTOR 10)" >> $datafile
x=1
mod=`awk -v x=$x 'NR==x' $INPUT_DIR/ListParticles`
export GPU=0
export BIOEM_ALGO=1
#export GPUWORKLOAD=100
#export BIOEM_CUDA_THREAD_COUNT=256
export BIOEM_DEBUG_BREAK=600
export BIOEM_DEBUG_NMAPS=1
export OMP_NUM_THREADS=1
export BIOEM_PROJECTIONS_AT_ONCE=1
export NP=96

# Running
mpiexec -perhost $NP $BIOEM --Modelfile $INPUT_DIR/CA-Ref_Nosym.pdb --ReadPDB --Inputfile $INPUT_DIR/Param_Input_AP-1 --Particlesfile $INPUT_DIR/IMAGES/$mod --ReadMRC --ReadOrientation $INPUT_DIR/LISTS_ORIENTATIONS/List_ori_$x >> $datafile

# Saving probabilities
echo "* EXP 3: SMALL BENCH PROBABILITIES" >> $datafile
cat Output_Probabilities >> $datafile

# Cleanup
rm -rf Output_Probabilities
unset BIOEM_ALGO GPU GPUWORKLOAD BIOEM_CUDA_THREAD_COUNT BIOEM_DEBUG_BREAK BIOEM_DEBUG_NMAPS OMP_NUM_THREADS BIOEM_PROJECTIONS_AT_ONCE NP

###################################
# Exp 4: Small benchmark, with CPU-only ALGO=2 config
echo "* EXP 4: SMALL BENCH OUTPUT (FACTOR 10)" >> $datafile
x=1
mod=`awk -v x=$x 'NR==x' $INPUT_DIR/ListParticles`
export GPU=0
export BIOEM_ALGO=2
#export GPUWORKLOAD=100
#export BIOEM_CUDA_THREAD_COUNT=256
export BIOEM_DEBUG_BREAK=600
export BIOEM_DEBUG_NMAPS=1
export OMP_NUM_THREADS=12
export BIOEM_PROJECTIONS_AT_ONCE=12
export NP=2

# Running
mpiexec -perhost $NP $BIOEM --Modelfile $INPUT_DIR/CA-Ref_Nosym.pdb --ReadPDB --Inputfile $INPUT_DIR/Param_Input_AP-1 --Particlesfile $INPUT_DIR/IMAGES/$mod --ReadMRC --ReadOrientation $INPUT_DIR/LISTS_ORIENTATIONS/List_ori_$x >> $datafile

# Saving probabilities
echo "* EXP 4: SMALL BENCH PROBABILITIES" >> $datafile
cat Output_Probabilities >> $datafile

# Cleanup
rm -rf Output_Probabilities
unset BIOEM_ALGO GPU GPUWORKLOAD BIOEM_CUDA_THREAD_COUNT BIOEM_DEBUG_BREAK BIOEM_DEBUG_NMAPS OMP_NUM_THREADS BIOEM_PROJECTIONS_AT_ONCE NP

###################################
# Exp 5: Small benchmark, with best ALGO=2 config
echo "* EXP 5: SMALL BENCH OUTPUT (FACTOR 10)" >> $datafile
x=1
mod=`awk -v x=$x 'NR==x' $INPUT_DIR/ListParticles`
export GPU=1
export BIOEM_ALGO=2
export GPUWORKLOAD=100
export BIOEM_CUDA_THREAD_COUNT=1024
export BIOEM_DEBUG_BREAK=600
export BIOEM_DEBUG_NMAPS=1
export OMP_NUM_THREADS=12
#export BIOEM_PROJECTIONS_AT_ONCE=1
export NP=2

# Running
mpiexec -perhost $NP $BIOEM --Modelfile $INPUT_DIR/CA-Ref_Nosym.pdb --ReadPDB --Inputfile $INPUT_DIR/Param_Input_AP-1 --Particlesfile $INPUT_DIR/IMAGES/$mod --ReadMRC --ReadOrientation $INPUT_DIR/LISTS_ORIENTATIONS/List_ori_$x >> $datafile

# Saving probabilities
echo "* EXP 5: SMALL BENCH PROBABILITIES" >> $datafile
cat Output_Probabilities >> $datafile

# Cleanup
rm -rf Output_Probabilities
unset BIOEM_ALGO GPU GPUWORKLOAD BIOEM_CUDA_THREAD_COUNT BIOEM_DEBUG_BREAK BIOEM_DEBUG_NMAPS OMP_NUM_THREADS BIOEM_PROJECTIONS_AT_ONCE NP
