#!/bin/bash
PATH=${HOME}/bin:${PATH}

module purge
module load gcc/$GCC_VER
module load lcov

for p in coverage_data/coverage_${CI_BUILD_REF}*.package ; do
        cov_file=$PWD/${p%.package}.info
        lcov -c -b $PWD --from-package $p --output-file $cov_file
        lcov -r $cov_file "/usr/*" -o $cov_file
        lcov -r $cov_file "/usr/include/*" -o $cov_file
        lcov -r $cov_file "*$GCC_VER*" -o $cov_file
        lcov -r $cov_file "/afs/*" -o $cov_file
        lcov -r $cov_file "include/MersenneTwister.h" -o $cov_file
        lcov -r $cov_file "include/autotuner.h" -o $cov_file
        lcov -r $cov_file "autotuner.cpp" -o $cov_file
done

lcov $(for i in coverage_data/coverage_${CI_BUILD_REF}_*.info; do echo -a $i; done) -o coverage_data/coverage_${CI_BUILD_REF}.info
lcov --summary coverage_data/coverage_${CI_BUILD_REF}.info 2>&1 | \
        awk '/lines|functions/ {
                gsub(/\.*:$/, "", $1);
                gsub(/^./, "", $3);
                printf "%s: %s (%s of %s), ", $1, $2, $3, $5;
             }
             /branches/ {
                print "";
             }' | \
        sed 's/^/__COVERAGE__:/; s/, $//;'

mkdir -p coverage_summary
genhtml coverage_data/coverage_${CI_BUILD_REF}.info -o coverage_summary
